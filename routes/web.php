<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;
// use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PagesController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/request', 'RequestController@create')->name('request');
// Route::post('/storeRequest', 'RequestController@store')->name('storeRequest');
Route::post('/storeRequest', 'RequestController@store')->name('storeRequest');
Route::get('/managerConfirm', 'RequestController@index')->name('managerConfirm');
// Route::get('/requestId/{request}', 'RequestController@show')->name('requestId');
Route::get('/requestId/{request}', 'RequestController@edit')->name('requestId');

// Manager Approving
// Route::post('/manager', 'ManagerController@store')->name('manager');
Route::patch('/requestId/{request}', 'RequestController@update')->name('manager');
Route::get('/mto', 'ManagerController@index')->name('mto');
// Route::get('/mtoView/{manager}', 'ManagerController@show')->name('mtoView');
Route::get('/mtoView/{manager}', 'ManagerController@edit')->name('mtoView');
Route::patch('/mtoView/{manager}', 'ManagerController@update')->name('mtoView');
// Route::post('/mtoAssign', 'MtoController@store')->name('mtoAssign');
// Route::patch('/mtoAssign/{mto}', 'ManagerController@update')->name('mtoAssign');

//Reginal Manager
Route::get('/rm', 'RmController@index')->name('rm');
// Route::get('/rmId/{rm}', 'RmController@show')->name('rmId');
Route::get('/rmId/{rm}', 'RmController@edit')->name('rmId');
Route::patch('/rmConfirm/{rm}', 'RmController@update')->name('rmConfirm');
//Permission Granted
Route::get('/permissions', 'PagesController@index')->name('permissions');
//Waiting list
Route::get('/pending', 'PagesController@pending')->name('pending');
Route::get('/pending/{pending}/pendingId', 'MtoController@edit')->name('pendingId');
Route::patch('/pending/{pending}', 'MtoController@update')->name('updateRequest');


//Users
Route::get('/editUser/{user}/editUser', 'HomeController@edit')->name('editUser/{user}/editUser');
Route::patch('/editUser/{user}', 'HomeController@update')->name('editUser');
Route::get('/showUser/{user}', 'HomeController@show')->name('showUser');
Route::delete('/userDelete/{user}', 'HomeController@destroy')->name('userDelete');


// Vehicles Routes
Route::post('/addVehicle', 'PagesController@storeVehicle')->name('addVehicle');
Route::delete('/deleteVehicle/{vehicle}', 'PagesController@destroy')->name('deleteVehicle');
