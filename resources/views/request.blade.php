@extends('layouts.layout')

@section('content')
@if (!Auth::guest() && Auth::user()->role=="Admin" || !Auth::guest() && Auth::user()->role=="DM" || !Auth::guest() &&
Auth::user()->role=="MTO" || !Auth::guest() && Auth::user()->role=="RM" || !Auth::guest() && Auth::user()->role=="stuff"
|| !Auth::guest() && Auth::user()->role=="driver")

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Make Request</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
    <hr>
</section>

<section class="content-header">
    @if (session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
    @endif
    <div class="container">
        <form action="/storeRequest" method="POST">
            @csrf
            <div class="col-md-12 row">
                <div class="col-md-3 form-group">
                    <label for="task">Task Alocated:</label>
                    <input type="text" name="task" value="{{old('task')}}" id="task" class="form-control" required>
                    @error('task')
                    <div class="text-danger">
                        This field is required
                    </div>
                    @enderror
                </div>
                <div class="col-md-3 form-group">
                    <label for="location">Location:</label>
                    <input type="text" name="location" value="{{old('location')}}" id="location" class="form-control"
                        required>
                    @error('location')
                    <div class="text-danger">
                        This field is required
                    </div>
                    @enderror
                </div>
                <div class="col-md-3">
                    <label for="date">Departure Date:</label>
                    <input type="date" name="date" value="{{old('date')}}" id="date" class="form-control" required>
                    @error('date')
                    <div class="text-danger">
                        This field is required
                    </div>
                    @enderror
                </div>
                <div class="col-md-3">
                    <label for="return">Return Date</label>
                    <input type="date" name="return_date" value="{{old('return_date')}}" id="" class="form-control">
                    @error('return_date')
                    <div class="text-danger">
                        This field is required
                    </div>
                    @enderror
                </div>
            </div>

            <div class="form-group">
                <label for="discription">Discription:</label>
                <textarea name="discription" id="discription" cols="30" rows="10" class="form-control"
                    required>{{old('discription')}}</textarea>
                @error('discription')
                <div class="text-danger">
                    This field is required
                </div>
                @enderror
            </div>

            <div class="container">
                <button class="btn btn-success" type="submit">Submit</button>
            </div>
        </form>
    </div>
</section>
@else

<section class="content">
    <div class="error-page">
        <h2 class="headline text-warning"> 404</h2>

        <div class="error-content">
            <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>
            <p>
                You are not an authorised User for this page!
            </p>
        </div>
    </div>
</section>

@endif
@endsection
