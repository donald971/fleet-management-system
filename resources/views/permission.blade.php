@extends('layouts.layout')

@section('content')
@if (!Auth::guest() && Auth::user()->role=="Admin" || !Auth::guest() && Auth::user()->role=="DM" || !Auth::guest() &&
Auth::user()->role=="MTO" || !Auth::guest() && Auth::user()->role=="RM" || !Auth::guest() && Auth::user()->role=="stuff"
|| !Auth::guest() && Auth::user()->role=="driver")
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Permission Granted</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
    <hr>
</section>

<?php
    $permissions = 1;
?>

<section>
    {{-- @if (!Auth::guest() && Auth::user()->name == $requests->user->name)
    Here you are!
    @endif --}}
    <div class="container">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">worker</th>
                    <th scope="col">task</th>
                    <th scope="col">location</th>
                    <th scope="col">Vehicle</th>
                    <th scope="col">Driver</th>
                    <th scope="col">Depature Date</th>
                    <th scope="col">Return Date</th>
                </tr>
            </thead>
            <tbody>
                @if (count($requests)>0)
                @foreach ($requests as $request)
                @if ($request->permission != '')
                @if (!Auth::guest() && Auth::user()->name == $request->user->name)
                <tr>
                    <th scope="col">{{$permissions++}}</th>
                    <td>{{$request->user->name}}</td>
                    <td>{{$request->task}}</td>
                    <td>{{$request->location}}</td>
                    <td>{{$request->vehicles->name}}</td>
                    <td>{{$request->driver}}</td>
                    <td>{{$request->date}}</td>
                    <td>{{$request->return_date}}</td>
                </tr>
                @endif
                @endif
                @endforeach
                @else
                <div class="alert alert-danger">
                    No Permission Granted yet!
                </div>
                @endif
            </tbody>
        </table>
    </div>
</section>
@else

<section class="content">
    <div class="error-page">
        <h2 class="headline text-warning"> 404</h2>

        <div class="error-content">
            <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>
            <p>
                You have no role to perform any task in our system.
                If you are a new member please contact Administrator to Activate your account!
            </p>
        </div>
    </div>
</section>

@endif
@endsection
