@extends('layouts.show')

@section('content')
@if (!Auth::guest() && Auth::user()->role=="RM")
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Regional Manager</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
    <hr>
</section>

<section>
    <div class="container">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">worker</th>
                    <th scope="col">task</th>
                    <th scope="col">location</th>
                    <th scope="col">Manager</th>
                    {{-- <th scope="col">Manager Transport</th> --}}
                    <th scope="col">Vehicle</th>
                    <th scope="col">Driver</th>
                    <th scope="col">Depature Date</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">{{$rm->id}}</th>
                    <td> {{$rm->user->name}} </td>
                    <td> {{$rm->task}} </td>
                    <td> {{$rm->location}} </td>
                    <td> {{$rm->manager}} : {{$rm->status}} </td>
                    {{-- Manager Transport Officer name has a problem when displaying infront for the users and so currently will be commentd --}}
                    {{-- <td> {{$rm->managers->mto_name}} </td> --}}
                    <td> {{$rm->vehicle}} </td>
                    <td> {{$rm->driver}} </td>
                    <td> {{$rm->date}} </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>

@if (session('success'))
<div class="alert alert-success">
    {{session('success')}}
</div>
@endif

@if ($rm->permission == '')
<section>
    <form action="../../rmConfirm/{{$rm->id}}" method="post">
        @csrf
        @method('PATCH')
        <div class="container">
            <div>
                <strong>
                    Approve
                </strong>
                <hr>
            </div>

            {{-- <div class="form-group">
                <input type="hidden" name="worker" value="{{$rm->managers->user_name}}" required>
            @error('worker')
            <div class="text-danger">
                This Field is required
            </div>
            @enderror
        </div>
        <div class="form-group">
            <input type="hidden" name="task" value="{{$rm->managers->task}}" required>
            @error('task')
            <div class="text-danger">
                This Field is required
            </div>
            @enderror
        </div>
        <div class="form-group">
            <input type="hidden" name="location" value="{{$rm->managers->location}}" required>
            @error('location')
            <div class="text-danger">
                This Field is required
            </div>
            @enderror
        </div>
        <div class="form-group">
            <input type="hidden" name="vehicle" value="{{$rm->vehicle}}" required>
            @error('vehicle')
            <div class="text-danger">
                This Field is required
            </div>
            @enderror
        </div>
        <div class="form-group">
            <input type="hidden" name="driver" value="{{$rm->driver}}" required>
            @error('driver')
            <div class="text-danger">
                This Field is required
            </div>
            @enderror
        </div>
        <div class="form-group">
            <input type="hidden" name="depature_date" value="{{$rm->depature_date}}" required>
            @error('depature_date')
            <div class="text-danger">
                This Field is required
            </div>
            @enderror
        </div>

        <div class="form-group">
            <input type="hidden" name="request_id" class="form-control" value="{{$rm->managers->requests->id}}">
            @error('request_id')
            <div class="text-danger">
                This Field is required
            </div>
            @enderror
        </div> --}}

        <div class="form-group">
            <select name="rm_approve" id="" class="form-control" required>
                <option value="">Choose</option>
                <option value="Aproved">Approve</option>
                <option value="Discard">Disapprove</option>
            </select>
            @error('rm_approve')
            <div class="text-danger">
                This Field is required
            </div>
            @enderror
        </div>

        <div class="form-group">
            <button class="btn btn-success">Approve</button>
        </div>
        </div>
    </form>
</section>
@else
<h3 class="text-success">
    Approved
</h3>
@endif
@else

<section class="content">
    <div class="error-page">
        <h2 class="headline text-warning"> 404</h2>

        <div class="error-content">
            <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>
            <p>
                You are not an authorised User for this page!
            </p>
        </div>
    </div>
</section>

@endif
@endsection
