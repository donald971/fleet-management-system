@extends('layouts.show')

@section('content')
@if (!Auth::guest() && Auth::user()->role=="Admin")
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit User</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
    <hr>
</section>

@if (session('success'))
<div class="alert alert-success">
    {{session('success')}}
</div>
@endif

<section>
    <div class="container">
        <form action="../{{$user -> id}}" method="post">
            @csrf
            @method('PATCH')
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="" value="{{$user->name}}" class="form-control">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" id="" value="{{$user->email}}" class="form-control">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="role">Role</label>
                        {{-- <input type="text" name="role" id="" value="{{$user->role}}" class="form-control"> --}}
                        <select name="role" id="" class="form-control">
                            <option value="{{$user->role}}">{{$user->role}}</option>
                            <option value="stuff">Stuff Member</option>
                            <option value="DM">Direct Manager</option>
                            <option value="RM">Regional MAnager</option>
                            <option value="MTO">Manager Transport Officer</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-success">Save Change</button>
            </div>
        </form>
    </div>
</section>
@else

<section class="content">
    <div class="error-page">
        <h2 class="headline text-warning"> 404</h2>

        <div class="error-content">
            <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>
            <p>
                You are not an authorised User for this page!
            </p>
        </div>
    </div>
</section>

@endif
@endsection
