@extends('layouts.show')

@section('content')
@if (!Auth::guest() && Auth::user()->role=="MTO")
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Manager Transport Officer</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
    <hr>
</section>

<section>
    <div class="container">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Worker</th>
                    <th scope="col">Task allocates</th>
                    <th scope="col">Location</th>
                    <th scope="col">Manager</th>
                    <th scope="col">Depature Date</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>{{$mto->id}}</th>
                    <td>{{$mto->user->name}}</td>
                    <td>{{$mto->task}}</td>
                    <td>{{$mto->location}}</td>
                    <td>
                        <strong>{{$mto->manager}} : </strong> {{$mto->status}}
                    </td>
                    <td>{{$mto->created_at}}</td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            Task Description:
                        </strong>
                    </td>
                    <td colspan="5">
                        <div>
                            {{$mto->discription}}
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>

@if (session('success'))
<div class="alert alert-success">
    {{session('success')}}
</div>
@endif
@if ($mto->vehicle_id == '' && $mto->driver == '' && $mto->approved == '')
<div>
    <div class="container">
        <strong>
            Assign
        </strong>
        <hr>
    </div>
</div>

<section>
    <div class="container">
        <form action="../../mtoView/{{$mto->id}}" method="post">
            @csrf
            @method('PATCH')
            {{-- <input type="hidden" name="manager_id" value="{{$mto->id}}">
            <input type="hidden" name="depature_date" value="{{$mto->depature_date}}"> --}}
            <div class="row col-md-12">
                <div class="col-md-4 form-group">
                    <label for="">Assign a Vehicle</label>
                    {{-- <input type="text" name="vehicle" class="form-control" required> --}}
                    <select name="vehicle" id="vehicle" class="form-control" required>
                        <option value="">--Select Vehicle--</option>
                        @foreach ($vehicles as $vehicle)
                        @if ($vehicle->status == '')
                        <option value="{{$vehicle->id}}">{{$vehicle->name}}</option>
                        @endif
                        @endforeach
                    </select>
                    @error('vehicle')
                    <div class="text-danger">
                        This field is required
                    </div>
                    @enderror
                </div>

                <div class="col-md-4 form-group">
                    <label for="">Assign Driver</label>
                    {{-- <input type="text" name="driver" class="form-control" required> --}}
                    <select name="driver" id="driver" class="form-control" required>
                        <option value="">--Select Driver--</option>
                        @foreach ($users as $user)
                        @if ($user->role == 'driver')
                        <option value="{{$user->name}}">{{$user->name}}</option>
                        @endif
                        @endforeach
                    </select>
                    @error('vehicle')
                    <div class="text-danger">
                        This field is required
                    </div>
                    @enderror
                </div>

                <div class="col-md-4 form-group">
                    <label for="Approve">Approve</label>
                    <select name="approve" id="Approve" class="form-control" required>
                        <option value="">Choose</option>
                        <option value="aprove">Aprove</option>
                    </select>
                    @error('vehicle')
                    <div class="text-danger">
                        This field is required
                    </div>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</section>
@else
<h3 class="text-success">
    Assigned
</h3>
@endif
@else

<section class="content">
    <div class="error-page">
        <h2 class="headline text-warning"> 404</h2>

        <div class="error-content">
            <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>
            <p>
                You are not an authorised User for this page!
            </p>
        </div>
    </div>
</section>

@endif
@endsection
