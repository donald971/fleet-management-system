@extends('layouts.layout')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
    <hr>
</section>

@if (!Auth::guest() && Auth::user()->role=="Admin" || !Auth::guest() && Auth::user()->role=="DM" || !Auth::guest() &&
Auth::user()->role=="MTO" || !Auth::guest() && Auth::user()->role=="RM" || !Auth::guest() && Auth::user()->role=="stuff"
|| !Auth::guest() && Auth::user()->role=="driver")

@if (!Auth::guest() && Auth::user()->role=="Admin")
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{count($users)}}</h3>

                        <p>Number of Users</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3>
                            {{count($requests)}}
                        </h3>

                        <p>Number of requests</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-paper-plane"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>
                            {{count($vehicles)}}
                        </h3>

                        <p>Number of vehicles</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-car"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
@endif

@if (!Auth::guest() && Auth::user()->role!="Admin")
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3>
                            {{count(Auth::user()->requests)}}
                        </h3>

                        <p>Number of requests</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-paper-plane"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>
                            {{count($vehicles)}}
                        </h3>

                        <p>Number of vehicles</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-car"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
@endif


<?php
    $no = 1;
    $vehicle_no = 1;
?>

<section>
    <div class="container">
        @if (!Auth::guest() && Auth::user()->role=="Admin")
        <div class="row col-md-12">
            <div class="col-md-8">
                @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @endif
                <div>
                    <h1>
                        Users
                    </h1>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Names</th>
                            {{-- <th scope="col">Email</th> --}}
                            <th scope="col">Role</th>
                            @if(!Auth::guest() && Auth::user()->role=="Admin")
                            <th scope="col">Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($users)>0)
                        @foreach ($users as $user)
                        <tr>
                            <th scope="row">{{ $no++ }}</th>
                            <td>{{$user->name}}</td>
                            {{-- <td>{{$user->email}}</td> --}}
                            <td>{{$user->role}}</td>
                            @if(!Auth::guest() && Auth::user()->role=="Admin")
                            <td>
                                <a href="/showUser/{{$user->id}}"><i class="fa fa-eye"></i></a> /
                                <a href="/editUser/{{$user->id}}/editUser"><i class="fa fa-edit text-success"></i></a> /
                                <form action="userDelete/{{$user->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    {{-- <i class="fa fa-trash text-danger"></i> --}}
                                    <button type="submit" style="border: none">
                                        <i class="fa fa-trash text-danger"></i>
                                    </button>
                                </form>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>

            <div class="col-md-4">
                @if (session('vehicle'))
                <div class="alert alert-success">
                    {{session('vehicle')}}
                </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h3 class="float-left">Vehicles</h3>
                        <button class="btn btn-primary btn-sm float-right" data-toggle="modal"
                            data-target="#modal-default">Add Vehicle <i class="fa fa-car"></i></button>
                    </div>
                    <div class="card-body">
                        @if (count($vehicles)>0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Plate</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            @foreach ($vehicles as $vehicle)
                            <tbody>
                                <tr>
                                    <td>{{$vehicle_no++}}</td>
                                    <td>{{$vehicle->name}}</td>
                                    <td>{{$vehicle->plate_no}}</td>
                                    <td>
                                        <form action="deleteVehicle/{{$vehicle->id}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            {{-- <i class="fa fa-trash text-danger"></i> --}}
                                            <button type="submit" style="border: none">
                                                <i class="fa fa-trash text-danger"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                        @else
                        <div class="alert alert-warning">
                            No Vehicles
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</section>

{{-- Add Vehicle Model --}}

<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Vehicle</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/addVehicle" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Vehicle Name</label>
                        <input type="text" name="name" id="" class="form-control">
                        @error('name')
                        <div class="text-danger">
                            This field is required
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="plate_no">Plate Number</label>
                        <input type="text" name="plate_no" id="" class="form-control">
                        @error('plate_no')
                        <div class="text-danger">
                            This field is required
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {{-- <a href="addVehicle"> --}}
                    <button type="submit" class="btn btn-primary">Add New Vehicle</button>
                    {{-- </a> --}}
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@else
<section class="content">
    <div class="error-page">
        <h2 class="headline text-warning"> 404</h2>

        <div class="error-content">
            <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>
            <p>
                You have no role to perform any task in our system.
                If you are a new member please contact Administrator to Activate your account!
            </p>
        </div>
    </div>
</section>

@endif

@endsection
