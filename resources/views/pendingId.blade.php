@extends('layouts.show')

@section('content')
@if (!Auth::guest() && Auth::user()->role=="MTO")
{{-- {{$pending}} --}}

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit Request</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
    <hr>
</section>

@if (session('success'))
<div class="alert alert-success">
    {{session('success')}}
</div>
@endif

<section>
    <div class="container">
        <form action="../{{$pending->id}}" method="post">
            @csrf
            @method('PATCH')
            <input type="hidden" name="manager_id" value="{{$pending->manager_id}}" required>
            <input type="hidden" name="depature_date" value="{{$pending->depature_date}}" required>
            <div class="row col-md-12">
                <div class="col-md-4 form-group">
                    <label for="">Assign a Vehicle</label>
                    <input type="text" name="vehicle" value="{{$pending->vehicle}}" class="form-control" required>
                    @error('vehicle')
                    <div class="alert alert-danger">
                        This field is required
                    </div>
                    @enderror
                </div>

                <div class="col-md-4 form-group">
                    <label for="">Assign Driver</label>
                    <input type="text" name="driver" value="{{$pending->driver}}" class="form-control" required>
                    @error('driver')
                    <div class="alert alert-danger">
                        This field is required
                    </div>
                    @enderror
                </div>

                <div class="col-md-4 form-group">
                    <label for="Approve">Approve</label>
                    <select name="approve" id="Approve" class="form-control" required>
                        <option value="{{$pending->approve}}">{{$pending->approve}}</option>
                        <option value="aprove">Aprove</option>
                        <option value="pending">Pending</option>
                    </select>
                    @error('approve')
                    <div class="alert alert-danger">
                        This field is required
                    </div>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
</section>
@else
<div class="alert alert-danger">
    You are not an authorised User for this page!
</div>
@endif
@endsection
