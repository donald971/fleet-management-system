@extends('layouts.layout')

@section('content')
@if (!Auth::guest() && Auth::user()->role=="DM")
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Confirm Requests</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
    <hr>
</section>

<?php
    $number = 1;
?>

<section>
    <div class="container">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Worker Name</th>
                    <th scope="col">Task Allocated</th>
                    <th scope="col">Depature Date</th>
                    <th scope="col">action</th>
                </tr>
            </thead>
            <tbody>
                @if (count($requests)>0)
                @foreach ($requests as $request)
                <tr>
                    <th scope="row">{{$number++}}</th>
                    <td>{{$request->user->name}}</td>
                    <td>{{$request->task}}</td>
                    <td>{{$request->date}}</td>
                    <td>
                        <a href="/requestId/{{$request->id}}">
                            <i class="fa fa-eye"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                @else
                <div class="alert alert-danger">
                    No Request Found Yet!
                </div>
                @endif
            </tbody>
        </table>
    </div>
</section>

@else

<section class="content">
    <div class="error-page">
        <h2 class="headline text-warning"> 404</h2>

        <div class="error-content">
            <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>
            <p>
                You are not an authorised User for this page!
            </p>
        </div>
    </div>
</section>

@endif
@endsection
