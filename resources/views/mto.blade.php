@extends('layouts.layout')

@section('content')
@if (!Auth::guest() && Auth::user()->role=="MTO" || !Auth::guest() && Auth::user()->role=="stuff")
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Manager Transport Officer</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
    <hr>
</section>

<section>
    <div class="container">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">worker</th>
                    <th scope="col">task</th>
                    <th scope="col">Manager</th>
                    <th scope="col">Departure Date</th>
                    <th scope="col">D.M</th>
                    <th scope="col">M.T.O</th>
                    <th scope="col">R.M</th>

                    @if (!Auth::guest() && Auth::user()->role=="MTO")
                    <th scope="col">action</th>
                    @endif
                </tr>
            </thead>
            {{-- {{$requests->status}} --}}
            <tbody>
                @if (count($requests)>0)
                @foreach($requests as $request)
                {{-- @if ($request->status) --}}
                @if (!Auth::guest() && Auth::user()->name == $request->user->name || !Auth::guest() &&
                Auth::user()->role == 'MTO')
                <tr>
                    <th scope="row">{{$request->id}}</th>
                    <td> {{$request -> user ->name}} </td>
                    <td> {{$request->task}} </td>
                    <td> {{$request->manager}} </td>
                    <td> {{$request->date}} </td>
                    @if ($request->status == null)
                    <td class="text-danger"> Pending... </td>
                    @else
                    <td class="text-success">Confirmed...</td>
                    @endif

                    {{-- @if ($request->status != '') --}}
                    @if ($request->vehicle_id == null)
                    <td class="text-danger"> Pending... </td>
                    @else
                    <td class="text-success">Assigned...</td>
                    @endif
                    {{-- @endif --}}

                    {{-- @if ($request->vehicle != '') --}}
                    @if ($request->permission == null)
                    <td class="text-danger"> Pending... </td>
                    @else
                    <td class="text-success">Aproved...</td>
                    @endif
                    {{-- @endif --}}

                    @if (!Auth::guest() && Auth::user()->role=="MTO")
                    <td>
                        <a href="/mtoView/{{$request->id}}">
                            <i class="fa fa-eye"></i>
                        </a>
                    </td>
                    @endif
                </tr>
                @endif
                {{-- @endif --}}
                @endforeach
                @else
                <div class="alert alert-danger">
                    No Request Found Yet!
                </div>
                @endif
            </tbody>
        </table>
    </div>
</section>
@else
<section class="content">
    <div class="error-page">
        <h2 class="headline text-warning"> 404</h2>

        <div class="error-content">
            <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>
            <p>
                You are not an authorised User for this page!
            </p>
        </div>
    </div>
</section>
@endif
@endsection
