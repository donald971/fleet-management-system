@extends('layouts.layout')

@section('content')
@if (!Auth::guest() && Auth::user()->role=="DM" || !Auth::guest() && Auth::user()->role=="stuff" || !Auth::guest() &&
Auth::user()->role=="Admin" || !Auth::guest() && Auth::user()->role=="MTO")
<section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Waiting List</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
        <hr>
    </section>


    <div class="container">
        <div class="container">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">worker</th>
                        <th scope="col">task</th>
                        <th scope="col">location</th>
                        <th scope="col">Manager</th>
                        <th scope="col">Depature Date</th>

                        @if (!Auth::guest() && Auth::user()->role=="DM" || !Auth::guest() &&
                        Auth::user()->role=="stuff" || !Auth::guest() &&
                        Auth::user()->role=="Admin" || !Auth::guest() && Auth::user()->role=="MTO")
                        <th scope="col">Status</th>
                        @endif

                        @if (!Auth::guest() && Auth::user()->role=="MTO")
                        <th scope="col">action</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @if ((count($pendings)>0))
                    @foreach($pendings as $pending)
                    @if ($pending->approve == "pending")
                    <tr>
                        <th scope="row">{{$pending->id}}</th>
                        <td> {{$pending->managers->user_name}} </td>
                        <td> {{$pending->managers->task}} </td>
                        <td> {{$pending->managers->location}} </td>
                        <td> {{$pending->managers->manager_name}} </td>
                        <td> {{$pending->managers->depature_date}} </td>
                        @if (!Auth::guest() && Auth::user()->role=="DM" || !Auth::guest() &&
                        Auth::user()->role=="stuff" || !Auth::guest() &&
                        Auth::user()->role=="Admin" || !Auth::guest() && Auth::user()->role=="MTO")
                        <td class="text-danger"> {{$pending->approve}}... </td>
                        @endif

                        @if (!Auth::guest() && Auth::user()->role=="MTO")
                        <td>
                            <a href="/pending/{{$pending->id}}/pendingId">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                        @endif

                    </tr>
                    {{-- @else
                    <div class="alert alert-danger">
                        No Pending Request
                    </div> --}}
                    @endif
                    @endforeach
                    @else
                    <div class="alert alert-danger">
                        No Request Found Yet!
                    </div>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</section>
@else
<div class="alert alert-danger">
    You are not an authorised User for this page!
</div>
@endif
@endsection
