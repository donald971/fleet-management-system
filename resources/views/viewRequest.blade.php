@extends('layouts.show')

@section('content')
@if (!Auth::guest() && Auth::user()->role=="DM")
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                {{-- <h1>Name : {{$requests->user->name}}</h1> --}}
                <h1>Direct Manager</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
    <hr>
</section>

<section>
    <div class="container">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">User Name</th>
                    <th scope="col">Task</th>
                    <th scope="col">Location</th>
                    <th scope="col">Depature Date</th>
                    <th scope="col">Requested date</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">{{$requests->id}}</th>
                    <td>{{$requests->user->name}}</td>
                    <td>{{$requests->task}}</td>
                    <td>{{$requests->location}}</td>
                    <td>{{$requests->date}}</td>
                    <td>{{$requests->created_at}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</section>

<section>
    @if (session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
    @endif

    @if ($requests->status == '')
    <div class="container-fluid">
        <form action="../../requestId/{{$requests->id}}" method="post">
            @csrf
            @method('PATCH')
            {{-- <div class="form-group">
                <input type="hidden" name="request_id" value="{{$requests->id}}" class="form-control" required>
    </div>
    <div class="form-group">
        <input type="hidden" name="user_name" value="{{$requests->user->name}}" class="form-control" required>
    </div>
    <div class="form-group">
        <input type="hidden" name="task" value="{{$requests->task}}" class="form-control" required>
    </div>
    <div class="form-group">
        <input type="hidden" name="location" value="{{$requests->location}}" class="form-control" required>
    </div>
    <div class="form-group">
        <input type="hidden" name="depature_date" value="{{$requests->date}}" class="form-control" required>
    </div>
    <div class="form-group">
        <input type="hidden" name="discription" value="{{$requests->discription}}" class="form-control" required>
    </div> --}}
    <div class="form-group">
        <select name="approve" id="" class="form-control" required>
            <option value="">Choose</option>
            <option value="Confirmed">Approve</option>
        </select>
        @error('approve')
        <div class="text-danger">
            This Field is required
        </div>
        @enderror
    </div>
    <div class="form-group">
        <button class="btn btn-success">Confirm</button>
    </div>
    </form>
    </div>
    @else
    <h3 class="text-success">
        Confirmed
    </h3>
    @endif
</section>
@else

<section class="content">
    <div class="error-page">
        <h2 class="headline text-warning"> 404</h2>

        <div class="error-content">
            <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>
            <p>
                You are not an authorised User for this page!
            </p>
        </div>
    </div>
</section>
@endif
@endsection
