@extends('layouts.layout')

@section('content')
@if (!Auth::guest() && Auth::user()->role=="RM")
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Regional Manager</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
    <hr>
</section>

<section>
    <div class="container">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">worker</th>
                    <th scope="col">task</th>
                    <th scope="col">location</th>
                    <th scope="col">Manager</th>
                    <th scope="col">Depature Date</th>
                    <th scope="col">action</th>
                </tr>
            </thead>
            <tbody>
                @if (count($rms)>0)
                @foreach($rms as $rm)
                @if ($rm->approved == "aprove")
                <tr>
                    <th scope="row">{{$rm->id}}</th>
                    <td> {{$rm->user->name}} </td>
                    <td> {{$rm->task}} </td>
                    <td> {{$rm->location}} </td>
                    <td> {{$rm->manager}} </td>
                    <td> {{$rm->date}} </td>
                    <td>
                        <a href="/rmId/{{$rm->id}}">
                            <i class="fa fa-eye"></i>
                        </a>
                    </td>
                </tr>
                @endif
                @endforeach
                @else
                <div class="alert alert-danger">
                    No Request Found Yet!
                </div>
                @endif
            </tbody>
        </table>
    </div>
</section>
@else

<section class="content">
    <div class="error-page">
        <h2 class="headline text-warning"> 404</h2>

        <div class="error-content">
            <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>
            <p>
                You are not an authorised User for this page!
            </p>
        </div>
    </div>
</section>

@endif
@endsection
