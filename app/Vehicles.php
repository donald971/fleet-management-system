<?php

namespace App;

use App\Requests;
// use App\Vehicles;

use Illuminate\Database\Eloquent\Model;

class Vehicles extends Model
{
    //Define relationship for Manager and Mto
    public function mto()
    {
        return $this->belongsTo(Mto::class);
    }

    //Define relationship for Manager and Mto
    public function requests()
    {
        return $this->hasMany(Requests::class);
    }

    //Define relationship for Manager and Mto
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
