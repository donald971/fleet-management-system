<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Managers;
use App\Requests;
use App\Mto;
use App\Rm;
use App\User;

class MtoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Filtering datas entered by user
        $data = request()->validate([
            'manager_id' => 'required',
            'depature_date' => 'required',
            'vehicle' => 'required',
            'driver' => 'required',
            'approve' => 'required'
        ]);
        //Storing data in to database
        $mtoAssigns = new Mto;
        $mtoAssigns->manager_id = $request->input('manager_id');
        $mtoAssigns->vehicle = $request->input('vehicle');
        $mtoAssigns->driver = $request->input('driver');
        $mtoAssigns->approve = $request->input('approve');
        $mtoAssigns->depature_date = $request->input('depature_date');
        // dd($mtoAssigns);
        $mtoAssigns->save();
        return redirect()->back()->with('success', 'Successfully Assigned!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pending = Mto::find($id);
        // dd($pending->id);
        return view('pendingId')->with('pending', $pending);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Filtering datas entered by user
        $data = request()->validate([
            'manager_id' => 'required',
            'depature_date' => 'required',
            'vehicle' => 'required',
            'driver' => 'required',
            'approve' => 'required'
        ]);
        // dd("After Filtering");
        //Saving the data to database
        $updateRequest = Mto::find($id);
        $updateRequest->manager_id = $request->input('manager_id');
        $updateRequest->depature_date = $request->input('depature_date');
        $updateRequest->vehicle = $request->input('vehicle');
        $updateRequest->driver = $request->input('driver');
        $updateRequest->approve = $request->input('approve');
        // dd($updateRequest);
        $updateRequest->save();
        return redirect()->back()->with('success', 'Informations updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
