<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Managers;
use App\Requests;
use App\Mto;
use App\Rm;
use App\User;
use App\Vehicles;

class RmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rms = Requests::all();
        // dd($rms);
        return view('rm')->with('rms', $rms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Filtering datas entered by user
        $data = request()->validate([
            'worker' => 'required',
            'task' => 'required',
            'request_id' => 'required',
            'location' => 'required',
            'vehicle' => 'required',
            'driver' => 'required',
            'depature_date' => 'required',
            'rm_approve' => 'required'
        ]);
        //Storing Data
        $rms = new Rm;
        $rms->worker = $request->input('worker');
        $rms->task = $request->input('task');
        $rms->location = $request->input('location');
        $rms->vehicle = $request->input('vehicle');
        $rms->driver = $request->input('driver');
        $rms->depature_date = $request->input('depature_date');
        $rms->requests_id = $request->input('request_id');
        $rms->rm_approve = $request->input('rm_approve');

        // dd($rms);
        $rms->save();
        return redirect()->back()->with('success', 'Permission Granted Successfuly!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $rm = Mto::with('managers.requests')->find($id);
        // dd($rm->managers->requests);
        // return view('rmId')->with('rm', $rm);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rm = Requests::with('user')->find($id);
        // dd($rm);
        return view('rmId')->with('rm', $rm);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Filtering datas entered by user
        $data = request()->validate([
            'rm_approve' => 'required'
        ]);

        //Saving Data to database
        $requests = Requests::find($id);
        $requests->permission = $request->input('rm_approve');
        // dd($requests);
        $requests->save();
        return redirect()->back()->with('success', 'Permission Granted Successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
