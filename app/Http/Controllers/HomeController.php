<?php

namespace App\Http\Controllers;

use App\Requests;
use Illuminate\Http\Request;
use App\User;
use App\Rm;
use App\Vehicles;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::with('requests.rm')->get();
        $requests = Requests::all();
        $vehicles = Vehicles::all();
        // dd($users);
        return view('home')->with('users', $users)->with('requests', $requests)->with('vehicles', $vehicles);
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('editUser')->with('user', $user);
    }

    //Updating user data
    public function update(Request $request, $id)
    {
        //Filtering datas entered by user
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required',
            'role' => 'required'
        ]);
        //Saving the data to database
        $user = User::find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role = $request->input('role');
        $user->save();
        return redirect()->back()->with('success', 'Informations updated successfully!');
    }

    public function show($id)
    {
        // dd("Inside");
        $user = User::with('requests.rm')->find($id);
        // dd($user->requests);
        return view('showUser')->with('user', $user);
    }

    public function destroy($id)
    {
        // dd("Inside");
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->with('success', 'User Deleted successfully!');
    }
}
