<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Managers;
use App\Requests;
use App\Mto;
use App\Rm;
use App\User;
use App\Vehicles;

class ManagerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd("Inside");
        // $requests = Requests::with('vehicles')->get();
        $requests = Requests::with('user')->get();
        $vehicles = Vehicles::all();
        // dd($requests);
        return view('mto')->with('requests', $requests)->with('vehicles', $vehicles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Filtering datas entered by user
        $data = request()->validate([
            'request_id' => 'required',
            'user_name' => 'required',
            'task' => 'required',
            'location' => 'required',
            'depature_date' => 'required',
            'discription' => 'required',
            'approve' => 'required'
        ]);
        //Storing data in to database
        $managers = new Managers;
        $managers->request_id = $request->input('request_id');
        $managers->user_name = $request->input('user_name');
        $managers->manager_name = Auth()->user()->name;
        $managers->task = $request->input('task');
        $managers->location = $request->input('location');
        $managers->depature_date = $request->input('depature_date');
        $managers->discription = $request->input('discription');
        $managers->mto_name = Auth()->user()->name;
        $managers->approve = $request->input('approve');
        // dd($managers);
        $managers->save();
        return redirect()->back()->with('success', 'Confirmed Successfuly!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd("Inside");
        $mto = Requests::with('user')->find($id);
        $vehicles = Vehicles::all();
        $users = User::all();
        // dd($vehicles);
        return view('mtoView')->with('mto', $mto)->with('vehicles', $vehicles)->with('users', $users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mto = Requests::with('vehicles.user')->find($id);
        $vehicles = Vehicles::all();
        $users = User::all();
        // dd($mto->vehicle_id);
        return view('mtoView')->with('mto', $mto)->with('vehicles', $vehicles)->with('users', $users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicles $vehicles, $id)
    {
        // dd("Inside");
        //Filtering datas entered by user
        $data = request()->validate([
            'vehicle' => 'required',
            'driver' => 'required',
            'approve' => 'required'
        ]);

        // $vehicle->status = $request->input('vehicle');
        // $vehicles->status = $data['vehicle'];
        // $vehicles = Vehicles::find($vehicles);
        // // dd($vehicles);
        // if ($vehicles['0'] == $vehicles) {
        //     $vehicles->status = 1;
        // }
        // dd($vehicles);
        // $vehicles->update($data);

        //Storing data to Database
        $requests = Requests::with('user')->find($id);
        $requests->vehicle_id = $request->input('vehicle');
        $requests->driver = $request->input('driver');
        $requests->approved = $request->input('approve');
        // dd($requests);
        $requests->save();
        return redirect()->back()->with('success', 'Assigned Successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
