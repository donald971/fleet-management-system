<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Managers;
use App\Requests;
use App\Mto;
use App\Rm;
use App\User;
use App\Vehicles;

class PagesController extends Controller
{
    public function index()
    {
        $requests = Requests::with('user')->get();
        // dd($requests->user->name);
        return view('permission')->with('requests', $requests);
    }

    public function pending()
    {
        $pendings = Mto::with('managers')->get();
        // dd($pendings);
        return view('pending')->with('pendings', $pendings);
    }

    public function edit2222($id)
    {
        $pending = Mto::find($id);
        // dd($pending->id);
        return view('pendingId')->with('pending', $pending);
    }

    //Updating Requests status
    public function update222(Request $request, $id)
    {
        //Filtering datas entered by user
        $data = request()->validate([
            'manager_id' => 'required',
            'depature_date' => 'required',
            'vehicle' => 'required',
            'driver' => 'required',
            'approve' => 'required'
        ]);
        //Saving the data to database
        $request = Mto::find($id);
        $request->manager_id = $request->input('manager_id');
        $request->depature_date = $request->input('depature_date');
        $request->vehicle = $request->input('vehicle');
        $request->driver = $request->input('driver');
        $request->approve = $request->input('approve');
        dd($request);
        $request->save();
        return redirect()->back()->with('success', 'Informations updated successfully!');
    }

    public function storeVehicle(Request $request)
    {
        // dd("Inside");
        //Filtering datas entered by user
        $data = request()->validate([
            'name' => 'required',
            'plate_no' => 'required'
        ]);

        // Saving data to database
        $vehicle = new Vehicles;
        $vehicle->name = $request->input('name');
        $vehicle->plate_no = $request->input('plate_no');
        // dd($vehicle);
        $vehicle->save();
        return redirect()->back()->with('vehicle', 'Vehicle Added successfully!');
    }

    public function destroy($id)
    {
        $vehicle = Vehicles::find($id);
        $vehicle->delete();
        return redirect()->back()->with('vehicle', 'Vehicle Deleted successfully!');
    }
}
