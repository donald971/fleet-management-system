<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Managers;
use App\Requests;
use App\Mto;
use App\Rm;
use App\User;

class RequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests = Requests::with('user')->get();
        // dd($requests);
        return view('managerConfirm')->with('requests', $requests);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('request');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Filtering datas entered by user
        $data = request()->validate([
            'task' => 'required',
            'location' => 'required',
            'date' => 'required',
            'return_date' => 'required',
            'discription' => 'required'
        ]);

        //Saving the data to database
        $requests = new Requests;
        // dd($request->user_id = Auth()->user()->id);
        $requests->user_id = Auth()->user()->id;
        $requests->task = $request->input('task');
        $requests->location = $request->input('location');
        $requests->date = $request->input('date');
        $requests->return_date = $request->input('return_date');
        $requests->discription = $request->input('discription');
        // dd($requests);
        $requests->save();
        return redirect()->back()->with('success', 'Request Sent Successfuly!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $requests = Requests::with('user')->find($id);
        // dd($requests);
        // return view('viewRequest')->with('requests', $requests);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $requests = Requests::with('user')->find($id);
        // dd($requests);
        return view('viewRequest')->with('requests', $requests);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = request()->validate([
            'approve' => 'required'
        ]);

        // dd("Inside");

        //Saving the data to database
        $requests = Requests::with('user')->find($id);
        $requests->manager = Auth()->user()->name;
        $requests->status = $request->input('approve');
        // dd($requests);
        $requests->save();
        return redirect()->back()->with('success', 'Confirmed Successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
