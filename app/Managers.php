<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Managers extends Model
{
    //Define relationship for Manager and Request
    public function requests()
    {
        return $this->belongsTo(Requests::class, 'request_id', 'id');
    }

    //Define relationship for Manager and Mto
    public function mto()
    {
        return $this->hasOne(Mto::class);
    }
}
