<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mto extends Model
{
    //Define relationship for Manager and Mto
    public function managers()
    {
        return $this->belongsTo(Managers::class, 'manager_id', 'id');
    }

    //Define relationship for Manager and Mto
    public function vehicles()
    {
        return $this->hasMany(Vehicles::class);
    }
}
