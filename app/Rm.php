<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Rm extends Model
{
    //Define relationship
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //Define relationship
    public function requests()
    {
        return $this->belongsTo(Request::class);
    }
}
