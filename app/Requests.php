<?php

namespace App;

use App\User;
use App\Vehicles;

use Illuminate\Database\Eloquent\Model;

class Requests extends Model
{
    //Define relationship for Request and user
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    //Define relationship for Request and Manager
    public function managers()
    {
        return $this->hasOne(Managers::class);
    }

    //Define relationship
    public function rm()
    {
        return $this->hasOne(Rm::class);
    }

    //Define relationship
    public function vehicles()
    {
        return $this->belongsTo(Vehicles::class, 'vehicle_id', 'id');
    }
}
