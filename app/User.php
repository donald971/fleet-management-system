<?php

namespace App;

use App\Requests;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    //Define relationship
    public function requests()
    {
        return $this->hasMany(Requests::class);
    }

    //Define relationship
    public function rm()
    {
        return $this->hasMany(Rm::class);
    }

    //Define relationship for Manager and Mto
    public function vehicles()
    {
        return $this->hasOne(Vehicles::class, 'vehicle_id', 'id');
    }
}
