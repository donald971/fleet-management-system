<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('task');
            $table->string('location');
            $table->date('date');
            $table->date('return_date');
            $table->mediumText('discription');
            $table->string('manager')->nullable();
            $table->string('status')->nullable();
            $table->integer('vehicle_id')->nullable();
            $table->string('driver')->nullable();
            $table->string('approved')->nullable();
            $table->string('permission')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
