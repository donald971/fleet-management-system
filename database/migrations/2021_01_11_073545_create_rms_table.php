<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('worker');
            $table->string('task');
            $table->string('location');
            $table->string('vehicle');
            $table->string('driver');
            $table->string('depature_date');
            $table->integer('requests_id');
            $table->string('rm_approve');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rms');
    }
}
