<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMtosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('manager_id');
            $table->string('vehicle');
            $table->string('driver');
            $table->string('approve');
            $table->string('depature_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mtos');
    }
}
